using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Animations;

public class Enemy_Shell : MonoBehaviour
{
    public int Health = 100;
    public Animator anim;
    public int damageAmount = 25;
    public Collider playerCollider;

    public int enemyKilled;

    void Start()
    {
        enemyKilled = 0;
        anim = GetComponent<Animator>();
    }

    public void TakeDamage(int damageAmount)
    {
        Health -= damageAmount;

        if (Health <= 0)
        {

            enemyKilled++;
            anim.SetTrigger("die");
            GetComponent<Collider>().enabled = false;
            
        } 
        else
        {
            anim.SetTrigger("damage");
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag=="Player")
        {
            FPController.instance.playerHealth -= damageAmount;
        }
    }

    public void DeadTrigger()
    {
        Destroy(this.gameObject);
    }
}
