using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletScript : MonoBehaviour
{
    public int damageAmount = 25;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            transform.parent = other.transform;
            other.GetComponent<Enemy_Shell>().TakeDamage(damageAmount);

        }
    }
}
