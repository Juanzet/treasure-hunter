﻿using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FPController : MonoBehaviour {

    #region camera movement variable
    public float speed = 6f;
	public float mouseSensitivity =5f;
	public float jumpSpeed = 10f;
	

	private float rotationLeftRight;
	private float verticalRotation;
	private float forwardspeed;
	private float sideSpeed;
	private float verticalVelocity; 
	private Vector3 speedCombined;
	//private CharacterController cc;

	private Camera cam;
	#endregion

	public TextMeshProUGUI playerHP;
	public int playerHealth = 100;
	public static FPController instance;

	private Enemy_Shell _enemy_Shell;

    private void Awake()
    {
		instance = this;
    }

    void Start () 
	{
		cam = GetComponentInChildren<Camera> ();
		//cc = GetComponent<CharacterController> ();
		Cursor.visible = false;
		playerHP.text = "+" + playerHealth;
	}
	
	
	void Update () 
	{
		WinCondition();
    }

	public void WinCondition()
	{
		if (_enemy_Shell.enemyKilled == 30) 
		{
			SceneManager.LoadScene("Win");
		}
    }

	public void TakeDamage(int damageAmount)
	{
		playerHealth -= damageAmount;

		if (playerHealth <= 0)
		{
			SceneManager.LoadScene("Lose");
		}
	}

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag=="Enemy")
        {
			playerHealth -= 10;
        }
    }
}
