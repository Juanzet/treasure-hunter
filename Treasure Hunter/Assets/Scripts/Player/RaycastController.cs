using UnityEngine;

public class RaycastController : MonoBehaviour
{
    public float range;
    public float force = 4f;
    public GameObject effect;
    public GameObject pointCast;

    private RaycastHit hit;

    void Update()
    {
        RayCastLogic();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(Camera.main.transform.position, pointCast.transform.forward * range);
    }

    private void RayCastLogic()
    {
        if (Input.GetMouseButtonDown(0))
        {

            if (Physics.Raycast(Camera.main.transform.position, pointCast.transform.forward, out hit, range))
            {
                GameObject _effect = Instantiate(effect, hit.point, Quaternion.identity);
                Destroy(_effect, 0.5f);

                if (hit.collider.GetComponent<Rigidbody>() != null)
                {
                    hit.collider.GetComponent<Rigidbody>().AddForce(hit.normal * force);
                }
            }
        }
    }
}
