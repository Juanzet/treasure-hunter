using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public Transform spawnPoint;
    public GameObject bullet;
    public float shotForce = 100f;
    public float shotRate = 0.7f;
    public int damageAmount = 25;
    public Animation anim;
    
    private float shotRateTime = 0;
     
    void Update()
    {
        if(Input.GetButton("Fire1"))
        {
            
            if (Time.time > shotRateTime)
            {
                GameObject newBullet;

                newBullet = Instantiate(bullet, spawnPoint.position, bullet.transform.rotation);

                newBullet.GetComponent<Rigidbody>().AddForce(spawnPoint.forward * shotForce);

                shotRateTime = Time.time + shotRate;

                Destroy(newBullet, 2f);
            }
        }
    }

}
