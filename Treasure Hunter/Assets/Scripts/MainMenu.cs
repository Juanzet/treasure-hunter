using UnityEngine.SceneManagement;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
   public void IniciarGame()
    {
        SceneManager.LoadScene("SceneNight");
    }

    public void Options()
    {
        SceneManager.LoadScene("Options");
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void HTP()
    {
        SceneManager.LoadScene("HTP");
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
