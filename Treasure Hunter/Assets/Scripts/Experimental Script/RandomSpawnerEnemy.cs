using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawnerEnemy : MonoBehaviour
{
    public Transform[] spawnPoint;
    public Transform[] spawnPoint2;
    public GameObject enemy;
    public GameObject enemy2;

    void Start()
    {
        InvokeRepeating("Spawn", 5, 8);
        InvokeRepeating("SpawnII", 15, 8);
    }    

    void Spawn()
    {
        int i = Random.Range(0, 3);
        Instantiate(enemy, spawnPoint[i].position, transform.rotation);
    }

    void SpawnII()
    {
        int i = Random.Range(0, 3);
        Instantiate(enemy2, spawnPoint2[i].position, transform.rotation);
    }
}
