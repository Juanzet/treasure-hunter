using UnityEngine.UI;
using UnityEngine;

public class SoundLogic : MonoBehaviour
{
    public Slider slider;
    public float sliderValue;
    public Image imageMute;
    
    void Start()
    {
        slider.value = PlayerPrefs.GetFloat("soundValue", 0.5f);
        AudioListener.volume = slider.value;
        CheckMute();
    }

    
  public void ChangeSlider(float value)
    {
        sliderValue = value;
        PlayerPrefs.SetFloat("soundValue", sliderValue);
        AudioListener.volume = slider.value;
        CheckMute();
    }

    public void CheckMute()
    {
        if(sliderValue == 0)
        {
            imageMute.enabled = true;
        }
        else
        {
            imageMute.enabled = false;
        }
    }
}
